from bs4 import BeautifulSoup as bs
import json
#First read the ground truths

ft = open('ground-truth-test-byarticle-20181207/ground-truth-test-byarticle-20181207.xml')
content = ft.read()
bs_content = bs(content, "lxml")
articles = bs_content.find_all("article")

truths = {}
urls = {}
for article in articles:
    article_id = article.get('id')
    label = article.get('hyperpartisan')
    url = article.get('url')
    truths[article_id] = label
    urls[article_id] = url
    print("article id={} label={}".format(article_id, label))

ft.close()




fp=open('articles-test-byarticle-20181207/articles-test-byarticle-20181207.xml')
fw = open("dataset_test.jsonl", "w")
content = fp.read()
bs_content = bs(content, "lxml")

articles = bs_content.find_all("article")

for article in articles:
    article_id = article.get('id')
    label = truths[article_id]
    text = article.text
    title =  article.get('title')
    url = urls[article_id]
    obj = {"id": article_id, "label": label, "text": text, "title": title,
           "url": url}
    fw.write(json.dumps(obj)+"\n")
fw.close()

